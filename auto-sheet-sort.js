import {
    ActorSheet5eCharacter
} from "../../systems/dnd5e/module/actor/sheets/character.js";


ActorSheet5eCharacter.prototype._prepareItems = function SortPrepareItems(data) {

    //This overwrites the entire _prepareItems function in systems/dnd5e/module/actor/sheets/character.js and is extremely hacky.
    //It does, however, allow me to easily keep this change in my system with a module.
    //Starts on line 42

    // Categorize items as inventory, spellbook, features, and classes
    const inventory = {
      weapon: { label: "Weapons", items: [], dataset: {type: "weapon"} },
      equipment: { label: "Equipment", items: [], dataset: {type: "equipment"} },
      consumable: { label: "Consumables", items: [], dataset: {type: "consumable"} },
      tool: { label: "Tools", items: [], dataset: {type: "tool"} },
      backpack: { label: game.i18n.localize("DND5E.ItemContainerHeader"), items: [], dataset: {type: "backpack"} },
      loot: { label: "Loot", items: [], dataset: {type: "loot"} }
    };

    // Partition items by category
    let [items, spells, feats, classes] = data.items.reduce((arr, item) => {
      item.img = item.img || DEFAULT_TOKEN;
      item.isStack = item.data.quantity ? item.data.quantity > 1 : false;
      item.hasUses = item.data.uses && (item.data.uses.max > 0);
      item.isOnCooldown = item.data.recharge && !!item.data.recharge.value && (item.data.recharge.charged === false);
      item.isDepleted = item.isOnCooldown && (item.data.uses.per && (item.data.uses.value > 0));
      item.hasTarget = !!item.data.target && !(["none",""].includes(item.data.target.type));
      if ( item.type === "spell" ) arr[1].push(item);
      else if ( item.type === "feat" ) arr[2].push(item);
      else if ( item.type === "class" ) arr[3].push(item);
      else if ( Object.keys(inventory).includes(item.type ) ) arr[0].push(item);
      return arr;
    }, [[], [], [], []]);

    // Apply active item filters
    items = this._filterItems(items, this._filters.inventory);
    spells = this._filterItems(spells, this._filters.spellbook);
    feats = this._filterItems(feats, this._filters.features);

    //********************************************************************************** THIS IS THE SORTING CODE *****************************************************/*
    items = items.sort(function (a, b) {
      return a.name.localeCompare( b.name );
    });
    spells = spells.sort(function (a, b) {
      return a.name.localeCompare( b.name );
    });
    feats = feats.sort(function (a, b) {
      return a.name.localeCompare( b.name );
    });
    //********************************************************************************** END SORTING CODE **************************************************************/

    // Organize Spellbook
    const spellbook = this._prepareSpellbook(data, spells);
    const nPrepared = spells.filter(s => {
      return (s.data.level > 0) && (s.data.preparation.mode === "prepared") && s.data.preparation.prepared;
    }).length;

    // Organize Inventory
    let totalWeight = 0;
    for ( let i of items ) {
      i.data.quantity = i.data.quantity || 0;
      i.data.weight = i.data.weight || 0;
      i.totalWeight = Math.round(i.data.quantity * i.data.weight * 10) / 10;
      inventory[i.type].items.push(i);
      totalWeight += i.totalWeight;
    }
    data.data.attributes.encumbrance = this._computeEncumbrance(totalWeight, data);

    // Organize Features
    const features = {
      classes: { label: "Class Levels", items: [], hasActions: false, dataset: {type: "class"}, isClass: true },
      active: { label: "Active", items: [], hasActions: true, dataset: {type: "feat", "activation.type": "action"} },
      passive: { label: "Passive", items: [], hasActions: false, dataset: {type: "feat"} }
    };
    for ( let f of feats ) {
      if ( f.data.activation.type ) features.active.items.push(f);
      else features.passive.items.push(f);
    }
    classes.sort((a, b) => b.levels - a.levels);
    features.classes.items = classes;

    // Assign and return
    data.inventory = Object.values(inventory);
    data.spellbook = spellbook;
    data.preparedSpells = nPrepared;
    data.features = Object.values(features);
  }

